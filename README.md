# Continuous Matrix Product States for Quantum Fields
One of the biggest obstacles to the study of quantum many-body systems is the curse of dimensionality. The number of parameters that describe a quantum many-body system is exponentially large in the system size (N). Tensor Networks achieve to reduce the number of parameters (from exponentially to polynomial) by restricting the Hilbert space to its physically relevant part according to the area-law entanglement. 

In this poster we introduce matrix product states and focus on their continuous limit to describe quantum field theories. We also present a boson model to test the accuracy of this continuous approach compared to the Bethe ansatz.


## What is avaliable in this repository?

In this repository one can find the following:

- the poster on Continuous Matrix Product States which was written by Apostolos Giovanakis 
- the presentation on Continuous Matrix Product States which was presented by Apostolos Giovanakis in the MCQST Summer Program on the 25th of August in Munich.
- a code in Python to approximate the ground energy density using CMPS as a variational ansatz in the Lieb Liniger model
- a code in Python to compute expectation values of different operatos using CMPS as a variational ansatz in the Lieb Liniger model

A code on the CMPS for fermions is going to be uploaded here as soon as possible.  

## References

- [ ] F. Verstraete and J. I. Cirac. Continuous Matrix Product States for Quantum Fields. Phys. Rev. Lett. 104, 190405 (2010)
- [ ] Sangwoo S. Chung, Kuei Sun, and C. J. Bolech. Matrix product ansatz for Fermi fields in one dimension. Phys. Rev. B 91, 121108(2015)
- [ ] Jutho Haegeman, J. Ignacio Cirac, Tobias J. Osborne, and Frank Verstraete. Calculus of continuous matrix product states, Phys. Rev. B 88, 085118 (2013)

- [ ] Ulrich Schollwoeck. The density-matrix renormalization group in the age of matrix product states. Annals of Physics 326, 96 (2011)
- [ ] Jacob C. Bridgeman, Christopher T. Chubb. Hand-waving and Interpretive Dance: An Introductory Course on Tensor Networks. J. Phys. A: Math. Theor. 50 223001 (2017)
- [ ] Roman Orus. A Practical Introduction to Tensor Networks: Matrix Product States and Projected Entangled Pair States, Annals of Physics 349 (2014) 117-158.

